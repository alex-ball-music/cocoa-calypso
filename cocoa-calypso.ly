\version "2.18.0"

\language "english"
\include "ajb-setup.ly"
theStaffSize = #19
\include "ajb-paper.ly"
\include "ajb-layout.ly"
\include "articulate.ly"

\header {
 dedication = \markup { \italic { "For David Cleugh" } }
 title = "Cocoa Calypso"
 composer = "Alex Ball"
 copyright = \markup { \override #'(baseline-skip . 3) \center-column {
  "© Alex Ball 2001, 2011. Released under a Creative Commons Attribution Non-Commercial 4.0 International"
  "Licence: http://creativecommons.org/licenses/by-nc/4.0/" } }
 % Remove default LilyPond tagline
 tagline = ##f
}

\paper {
 #(set-paper-size "a4")
}

global = {
 \key c \major
 \time 8/8
 \tempo "With momentum" 4 = 120
 \partial 4.
}

sopranoVoice = \relative c'' {
 \global
 \dynamicUp
 % Music follows here.
 r8 r4
 R1
 r2. r8 g8\mf
 \repeat volta 3 {
  c4 d e d
  c8 a4 g4. r8 g8
  a4 a b b
  c8 d4 e4. r8 \parenthesize g,8
  c4 d e d
  c8 a4 g4. r8 g8
  a4 a b b
  c8 d4 e4. g4 \bar "||"
  % Chorus
  a4 f r2
  g4 e r2
  d8 d d e f f e d
  e8 d e f g f g( gs)
  a4 f r2
  g4 e r2
  d8 d d e f f e d
 }
 \alternative {
  {
   c4 c'8 c4 r8 r4
   R1
   r2. r8 g,8
  }{
  c4 r8 e4\mp r8 e8 e
  }
 }
 f8 f f f f f f f
 e8 e e e e r e e
 d8 d d e f f e d
 e8 d e f g r g g
 a8 a a g f g a f
 g8 f e f g r e e
 \override TextSpanner.bound-details.left.text = "rall. e cresc."
 d8\startTextSpan d d e fs d e fs
 g8 fs g a b2\stopTextSpan
 \break
 \once \override NoteHead.style = #'cross
 g1_\markup {
  \column {
   { "Or drizzling some chocolate sauce over it, Or stirring in chopped up pieces of chocolate bar," }
   { "Or sprinkling chocolate shavings over the top, Or coating the cone itself with chocolate," }
   { "All of which shows just how versatile that lovely foodstuff is that we call . . ." }
   { " " }
  }
 }
 \pageBreak
 \mark\markup{ \hspace #12 { \bold { "A tempo" } } }
 R1
 a4\f f a c
 g4 e g c
 d,8 d d e f f e d
 e8 d e g \tuplet 3/2 { bf4 a g }
 a4 f a c
 g4 e g c
 d,8 d d e f f e d
 c4 r8 r4.r4
 d8 d d e f g a b
 c4 c8\staccato c\staccato c\staccato \bar "|."
}

chorus = \lyricmode {
 choc’ -- late! Choc’ -- late!
 Ne -- ver will I e’er dis -- dain
 a bar of white or milk or plain
 choc’ -- late! Choc’ -- late!
 \dropLyrics
 Ic -- ky stic -- ky fin -- ger -- fuls of
}

bridge = \lyricmode {
 fun! Fun! Fun!
 \revertLyrics
}

verseFour = \lyricmode {
 fun! Yeah!

 You can spread it in your sand -- wich,
 you can drink it from a cup
 If you feel like ce -- le -- bra -- ting
 or you just need cheer -- ing up.
 You can mould it to make Eas -- ter eggs
 or bake it in a cake,
 You can li -- ven up an ice -- cream cone
 by for -- cing in a flake.

 \sh

 choc’ -- late! Choc’ -- late! Choc’ -- late! Choc’ -- late!
 Ne -- ver will I e’er dis -- dain
 a bar of white or milk or plain
 choc’ -- late! Choc’ -- late! Choc’ -- late! Choc’ -- late!
 Ic -- ky stic -- ky fin -- ger -- fuls of fun!
 Ic -- ky stic -- ky fin -- ger -- fuls of fun!
 Co -- co -- coa!
}

verseOne = \lyricmode {
 \set stanza = "1."
 % Lyrics follow here.
 The Az -- tecs were not ve -- ry nice:
 They prac -- tised hu -- man sac -- ri -- fice.
 \sh But they had one sa -- ving grace:
 It’s thanks to them we stuff our face

 With
}

verseTwo = \lyricmode {
 \set stanza = "2."
 % Lyrics follow here.
 "(Now)" co -- coa start -- ed off in life
 As how a lad se -- cured a wife;
 But once in Eu -- rope it was hip
 To blow your sa -- vings on a sip

 Of \chorus\bridge

 \set stanza = "2."
 Now

 \verseFour
}

verseThree = \lyricmode {
 \set stanza = "3."
 % Lyrics follow here.
 "(The)" Eight -- een Hun -- dreds saw the start
 Of mass pro -- duc -- tion choc’ -- late bars.
 The in -- dus -- try has not looked back
 Be -- cause we eat pack af -- ter pack

 Of % \chorus\bridge
 \repeat unfold 34 { \sh }

 \set stanza = "3."
 The
}

right = \relative c'' {
 \global
 % Music follows here.
 r4.
 r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
 r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
 \repeat volta 3 {
  r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
  r4 <f c a>8 r4 <d b g>8 r8 <d b g>8
  r4 <f c a>8 r4 <d b g>8 r8 <d b g>8
  r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
  r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
  r4 <f c a>8 r4 <d b g>8 r8 <d b g>8
  r4 <f c a>8 r4 <d b g>8 r8 <d b g>8
  r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
  % Chorus
  <a f c>4 <f c> <a' f c> <f c>
  <g, e c>4 <e c> <g' e c> <e c>
  <d, b g>8 <d b g> r8 <e c g> <f d b> <f d b> <e b g> <d b g>
  <e c g>8 <d b g> r8 <f d> <g e bf> <f d bf> <g e bf> <gs e bf>
  <a f c>4 <f c> <a' f c> <f c>
  <g, e c>4 <e c> <g' e c> <e c>
  <d, b g>8 <d b g> r8 <e c g> <f d b> <f d b> <e b g> <d b g>
 }
 \alternative {
  {
   <c g e>4 <c' c,>8 <c c,>4 r8 r4
   r4 <e, c g>8 r4 <e c g>8 r8 <e c g>8
   r4 <e c g>8 r4 <e c g>8 r8 <e c g>8
  } {
  <c g e>4 r8 <e c bf g>4 r8 <e c bf g>4
  }
 }
 <f c a>8 r4 r4. <f c a>4
 <e c g>8 r4 r4. <e c g>4
 <d b g>8 r4 r4. <d b g f>4
 <e c g>8 r4 <e c bf g>4 r8 <e c bf g>4
 <f c a>8 r4 r4. <f c a>4
 <e c g>8 r4 r4. <e cs a>4
 <d a fs>4. <e a,>8 <fs d a> <d a> <e a,> <fs d a>
 <g d b>2 <g d b>2\arpeggio
 R1
 c8\mp\< g e c e g bf4\f\!\noBreak
 <a f c>4 <f c> <a' f c> <f c>
 <g, e c>4 <e c> <g' e c> <e c>
 <d, b g>8 <d b g> r8 <e c g> <f d b> <f d b> <e b g> <d b g>
 <e c g>8 <d b g> <e c g>8 <g d> \tuplet 3/2 { <bf g e>4 <a e c> <g e c> }
 <a f c>4 <f c> <a' f c> <f c>
 <g, e c>4 <e c> <g' e c> <e c>
 <d, b g>8 <d b g> r8 <e c g> <f d b> <f d b> <e b g> <d b g>
 <c g e>8 <g e> <g e> <a f> <bf g> <bf g> <a f> <g e>
 <d' b g>8 <d b g> <d b g> <e c g> <f d> <g d b> <a f d> <b g d>
 <c g e>4 <c c,>8\staccato <c c,>\staccato <c c,>\staccato
}

left = \relative c' {
 \global
 % Music follows here.
 g8^\mf_\markup { \italic { "8vb ad lib." } } a b
 c4. g4 r8 g4
 c4. g4 r8 g4
 \repeat volta 3 {
  c4. g4 r8 g4
  f4. g4 r8 g4
  d'4. g,4 r8 g4
  c4. g4 r8 g4
  c4. g4 r8 g4
  f4. g4 r8 g4
  d'4. g,4 r8 g4
  c4. e4 r8 e4
  % Chorus
  f4 r8 f,4. a4
  c4 r8 e4. c4
  g4 r8 b4. g4
  c4 bf c g
  f4 r8 f4. a4
  c4 r8 e4. c4
  g4. g8 g g a b
 }
 \alternative {
  {
   c4 r8 r4 g8 a b
   c4. g4 r8 g4
   c4. g4 r8 g4
  }{
  c4 r8 c4^\mp r8 c4
  }
 }
 f,8 r4 r4. f4
 c'8 r4 r4. c4
 g8 r4 r4. g4
 c8 r4 c4 r8 c4
 f,8 r4 r4. f4
 c'8 r4 r4. a4
 \override TextSpanner.bound-details.left.text = "rall. e cresc."
 d4.\startTextSpan d4 c8 b a
 g2 g2\stopTextSpan
 R1
 R1
 f'4 r8 f,4. a4
 c4 r8 e4. c4
 g4 r8 b4. g4
 c4 bf c g
 f4 r8 f4. a4
 c4 r8 e4. c4
 g4. g8 g g a b
 c4. e,4. f4
 g4. g8 g g g g
 c4 r4 c8\staccato
}

sopranoVoicePart = \new Staff \with {
 instrumentName = "Solo"
 midiInstrument = "choir aahs"
} { \sopranoVoice }
\addlyrics { \verseOne}
\addlyrics { \verseTwo }
\addlyrics { \verseThree }

pianoPart = \new PianoStaff \with {
 instrumentName = "Piano"
} <<
 \new Staff = "right" \with {
  midiInstrument = "acoustic grand"
 } \right
 \new Staff = "left" \with {
  midiInstrument = "acoustic grand"
 } { \clef bass \left }
>>

\score {
  \transpose c' ef {
    <<
     \sopranoVoicePart
     \pianoPart
    >>
  }
  \layout {
    \context {
      \Score
      \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8)
    }
  }
}

\score {
  \transpose c' ef {
    \unfoldRepeats \articulate
    <<
      \sopranoVoicePart
      \pianoPart
    >>
  }
  \midi { }
}