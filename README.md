# Cocoa Calypso

I wrote this song in 2001 for one of my university friends, who was and is very
fond of chocolate. It was originally arranged for performance by the *a capella*
comedy quintet The Quadwranglers, and subsequently rearranged in 2011 for solo
voice and piano.

In lieu of any decent jokes or better ideas, I did some research into the
history of chocolate and based the first three verses around that. The final
verse is a comment on fact that they seem to put chocolate in everything these,
from hot cross buns to dessert wines. I even had some supposedly chocolate tea
once (it tasted more like school-dinners custard). In the light of this, it
seems somewhat ironic that, despite the name, the song turned out to be more of
a cha-cha.

## Summary information – solo version

  - *Voicing:* Solo voice

  - *Notes on ambitus:* 1.5 octaves, from B flat up to E flat.

  - *Instrumentation:* Piano

  - *Approximate performance length:* 2:40

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/cocoa-calypso/-/releases).
